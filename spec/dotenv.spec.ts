import { assert } from "chai";

describe("DotEnv Test", async () => {
  it("Basic DotEnv Test", async () => {
    process.env.DOTENV_CONFIG_PATH = "../spec/test.env";
    assert.isUndefined(process.env.TestEnvVarKey);
    assert.isUndefined(process.env.TestEnvVarKey2);
    require("../src/index.ts");
    assert.equal(process.env.TestEnvVarKey, "TestEnvVarValue");
    assert.equal(process.env.TestEnvVarKey2, "TestEnvVarValue2");
  });
});
