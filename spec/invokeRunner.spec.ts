import { Config, Service } from "mangaotaku-configurapi";
import { assert } from "chai";

import { InvokeRunner } from "../src/entities/invokeRunner";

describe("InvokeRunner", async () => {
  const service = new Service(Config.load("./invoke-config.yaml"));
  service.on("error", () => {});
  const runner = new InvokeRunner(service);
  [
    {
      name: "Basic Event Test",
      data: {
        event: {
          invoke: true,
          headers: { "Content-Type": "application/json" },
          name: "some_event",
          payload: {
            id: "id",
            key: "value",
          },
        },
        context: {},
      },
      expectedResponse: {
        statusCode: 200,
        headers: { "Content-Type": "application/json" },
        body: '{"id":"id","key":"value"}',
        isBase64Encoded: false,
      },
    },
    {
      name: "Basic Endpoint Error Test",
      data: {
        event: {
          invoke: true,
          headers: { "Content-Type": "application/json" },
          name: "some_event_error",
          payload: {
            id: "id",
            key: "value",
          },
        },
        context: {},
      },
      expectedResponse: {
        statusCode: 403,
        body: '{"statusCode":403,"message":"Error Response","details":""}',
        headers: { "Content-Type": "application/json" },
        isBase64Encoded: false,
      },
    },
  ].map(async (test) =>
    it(test.name, async () => {
      assert.deepEqual(
        await runner.execute(test.data.event, test.data.context),
        test.expectedResponse
      );
    })
  );
});
