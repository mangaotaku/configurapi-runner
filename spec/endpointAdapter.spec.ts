import { Response } from "mangaotaku-configurapi";
import { assert } from "chai";
import { EndpointAdapter } from "../src/entities/endpointAdapter";
import { getEventContext } from "./testHelper";

describe("EndpointAdapter", async () => {
  it("Basic Get Event", async () => {
    const { event, context } = getEventContext("/something", "./endpointEventContext.json");
    const request = new EndpointAdapter().toRequest(event, context);

    assert.isUndefined(request.name);
    assert.deepEqual(request.headers, {
      "x-amzn-tls-cipher-suite": "ECDHE-RSA-AES128-GCM-SHA256",
      "x-amzn-tls-version": "TLSv1.2",
      "x-amzn-trace-id": "Root=1-63bebbed-2bff90bf058c13cb34816fc8",
      "x-forwarded-proto": "https",
      host: "pl7xqdmr33zfjyjddjugkqjq3u0eejzx.lambda-url.us-east-1.on.aws",
      "x-forwarded-port": "443",
      "x-forwarded-for": "24.254.5.167",
      accept: "*/*",
      "user-agent": "curl/7.81.0",
    });
    assert.equal(request.path, "/something");
    assert.equal(request.method, "get");
    assert.isUndefined(request.payload);
    assert.equal(request.pathAndQuery, "/something");
  });

  it("Basic Post Event", async () => {
    const { event, context } = getEventContext("/object", "./endpointEventContext.json");
    const request = new EndpointAdapter().toRequest(event, context);

    assert.isUndefined(request.name);
    assert.deepEqual(request.headers, {
      "x-amzn-tls-cipher-suite": "ECDHE-RSA-AES128-GCM-SHA256",
      "x-amzn-tls-version": "TLSv1.2",
      "x-amzn-trace-id": "Root=1-63bebbed-2bff90bf058c13cb34816fc8",
      "x-forwarded-proto": "https",
      "content-type": "application/json",
      host: "pl7xqdmr33zfjyjddjugkqjq3u0eejzx.lambda-url.us-east-1.on.aws",
      "x-forwarded-port": "443",
      "x-forwarded-for": "24.254.5.167",
      accept: "*/*",
      "user-agent": "curl/7.81.0",
    });
    assert.equal(request.path, "/object");
    assert.equal(request.method, "post");
    assert.deepEqual(request.payload, { key: "value" });
    assert.equal(request.pathAndQuery, "/object?ohyeah=today");
    assert.deepEqual(request.query, { ohyeah: "today" });
  });

  it("Basic Response", async () => {
    const response = new EndpointAdapter().toResponse(
      new Response({ key: "value", key1: ["value1", "value2"], key3: false }, 200, {
        "content-type": "application/json",
      })
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(response.headers, { "content-type": "application/json" });
    assert.isUndefined(response.multiValueHeaders);
    assert.deepEqual(response.body, '{"key":"value","key1":["value1","value2"],"key3":false}');
    assert.equal(response.isBase64Encoded, false);
  });

  it("ELB Response", async () => {
    const response = new EndpointAdapter().toResponse(
      {
        ...new Response(
          {
            key: "value",
            key1: new Set(["value1", "value2", "value1"]),
            key3: false,
          },
          200,
          { "content-type": "application/json" }
        ),
        ...{
          jsonReplacer: (key, value) => (value instanceof Set ? [...value] : value),
        },
      },
      { elbProvider: true }
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(response.headers, { "content-type": "application/json" });
    assert.deepEqual(response.multiValueHeaders, {
      "content-type": ["application/json"],
    });
    assert.equal(response.statusDescription, `200 OK`);
    assert.deepEqual(response.body, '{"key":"value","key1":["value1","value2"],"key3":false}');
    assert.equal(response.isBase64Encoded, false);
  });

  it("Basic Response - string body", async () => {
    const response = new EndpointAdapter().toResponse(
      new Response("bananabanana", 200, { "content-type": "plain/text" })
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(response.headers, { "content-type": "plain/text" });
    assert.isUndefined(response.multiValueHeaders);
    assert.equal(response.body, "bananabanana");
    assert.equal(response.isBase64Encoded, false);
  });

  it("Basic Response - number body", async () => {
    const response = new EndpointAdapter().toResponse(
      new Response(1337, 200, { "content-type": "plain/text" })
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(response.headers, { "content-type": "plain/text" });
    assert.isUndefined(response.multiValueHeaders);
    assert.equal(response.body, "1337");
    assert.equal(response.isBase64Encoded, false);
  });

  it("Basic Response - buffer body", async () => {
    const response = new EndpointAdapter().toResponse(
      new Response(Buffer.from("hahahaha"), 200, {
        "content-type": "plain/text",
      })
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(response.headers, { "content-type": "plain/text" });
    assert.isUndefined(response.multiValueHeaders);
    assert.equal(response.body, Buffer.from("hahahaha").toString("base64"));
    assert.equal(response.isBase64Encoded, true);
  });

  it("Basic Response - no body", async () => {
    const response = new EndpointAdapter().toResponse(
      new Response(undefined, 200, { "content-type": "plain/text" })
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(response.headers, { "content-type": "plain/text" });
    assert.isUndefined(response.multiValueHeaders);
    assert.isEmpty(response.body, "");
    assert.equal(response.isBase64Encoded, false);
  });
});
