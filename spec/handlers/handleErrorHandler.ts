import { IEvent } from "mangaotaku-configurapi";
import { JsonResponse } from "configurapi-handler-json";

export async function handleErrorHandler(event: IEvent) {
  event.response = new JsonResponse(
    event.response.body as any,
    event.response.statusCode,
    event.response.headers
  );
  return this.complete();
}
