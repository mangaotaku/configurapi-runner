module.exports = function setResponseStatusCodeHandler(event, statusCode) {
  event.response.statusCode = statusCode;
};
