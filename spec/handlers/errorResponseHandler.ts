import { ErrorResponse, IEvent } from "mangaotaku-configurapi";

export async function errorResponseHandler(event: IEvent) {
  event.response = new ErrorResponse("Error Response", 403);
  return this.complete();
}
