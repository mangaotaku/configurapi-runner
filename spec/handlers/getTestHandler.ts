import { IEvent } from "mangaotaku-configurapi";
import { JsonResponse } from "configurapi-handler-json";

export async function getTestHandler(event: IEvent) {
  event.response = new JsonResponse({ id: "id", key: "value" });
  return this.continue();
}
