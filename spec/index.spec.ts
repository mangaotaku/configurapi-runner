import { execFileSync } from "child_process";
import { readdirSync } from "fs";
describe("Index", () => {
  it("Test sub tests", async () => {
    const files = readdirSync("./index");
    for (const file of files) {
      execFileSync("npm", ["run", "index-test", `./index/${file}`], { stdio: "inherit" });
    }
  }).timeout(60000);
});
