import { Config, Service } from "mangaotaku-configurapi";
import { assert } from "chai";
import { Message, Producer } from "sqs-producer";
import { IMock, It, Mock } from "typemoq";

import { SqsAdapter } from "../src/entities/sqsAdapter";
import { SqsRunner } from "../src/entities/sqsRunner";

describe("SqsRunner", async () => {
  const adapter = new SqsAdapter();
  const service = new Service(Config.load("./sqs-config.yaml"));
  let queueProducer: IMock<Producer>;
  let errorProducer: IMock<Producer>;
  let messages: Message[] = [];
  let errorMessages: Message[] = [];

  const exampleRequest = {
    Records: [
      {
        messageId: "d0aa20bc-37f2-43f7-9d7a-af24fe6bb9d6",
        receiptHandle:
          "AQEBqr/dlm7c3bvaM9LUF73kFB5CpE28Jb9G3oFXOADZbUf4C1uKbM8ez8OGvcC97jPGnZ/ZhUEMEtrny2Gx1MXWt15yzH+RiVZdiSBLK/VfRTNOBV/CizPDCByka5n7wFamrOrNEB5nqTRriMSeWVVRwrRdcLIbl72b6MO/8/TB7JH2R0rpRxBmRAtQWlXuP13ejS2nBgccv9LMfiUg6kZzk711ADxAjJKbGehb4wpoGpbE396fCVAjNSsA2PDPadqMl1ALl5MfohO4QDKoClYUe+HX1f7AEziFwRb4BJRbhaWzKLNEDNW1DQMyYp/Zby4zL/hJnjb3BFL/VBymdrDdaDtSbi6DW8/LIol+p6kma8RU/ADcQhZFIAI++dQTxcih",
        body: '{\n"name":"health_check",\n"payload":{"key":"value"}\n\n}',
        attributes: {
          ApproximateReceiveCount: "1",
          SentTimestamp: "1673542779372",
          SenderId: "senderId",
          ApproximateFirstReceiveTimestamp: "1673542779382",
        },
        messageAttributes: {},
        md5OfBody: "6c2778eeaf1583f669740ca0d229c4ba",
        eventSource: "aws:sqs",
        eventSourceARN: "arn:aws:sqs:us-east-1:123456789012:TestQueue",
        awsRegion: "us-east-1",
      },
    ],
  };

  const willFailRequest = {
    Records: [
      {
        messageId: "d0aa20bc-37f2-43f7-9d7a-af24fe6bb9d6",
        receiptHandle:
          "AQEBqr/dlm7c3bvaM9LUF73kFB5CpE28Jb9G3oFXOADZbUf4C1uKbM8ez8OGvcC97jPGnZ/ZhUEMEtrny2Gx1MXWt15yzH+RiVZdiSBLK/VfRTNOBV/CizPDCByka5n7wFamrOrNEB5nqTRriMSeWVVRwrRdcLIbl72b6MO/8/TB7JH2R0rpRxBmRAtQWlXuP13ejS2nBgccv9LMfiUg6kZzk711ADxAjJKbGehb4wpoGpbE396fCVAjNSsA2PDPadqMl1ALl5MfohO4QDKoClYUe+HX1f7AEziFwRb4BJRbhaWzKLNEDNW1DQMyYp/Zby4zL/hJnjb3BFL/VBymdrDdaDtSbi6DW8/LIol+p6kma8RU/ADcQhZFIAI++dQTxcih",
        body: '{\n"name":"fail",\n"payload":{"key":"value"}\n\n}',
        attributes: {
          ApproximateReceiveCount: "1",
          SentTimestamp: "1673542779372",
          SenderId: "senderId",
          ApproximateFirstReceiveTimestamp: "1673542779382",
        },
        messageAttributes: {},
        md5OfBody: "6c2778eeaf1583f669740ca0d229c4ba",
        eventSource: "aws:sqs",
        eventSourceARN: "arn:aws:sqs:us-east-1:123456789012:TestQueue",
        awsRegion: "us-east-1",
      },
    ],
  };

  const exampleLambdaInvocation = {
    httpMethod: "GET",
    path: "/",
    queryStringParameters: {},
    headers: {
      "user-agent": "ELB-HealthChecker/2.0",
    },
    body: '{\n"name":"health_check",\n"payload":{"key":"value"}\n\n}',
    isBase64Encoded: false,
  };

  const exampleContext = {
    callbackWaitsForEmptyEventLoop: true,
    functionVersion: "$LATEST",
    functionName: "ttest",
    memoryLimitInMB: "128",
    logGroupName: "/aws/lambda/ttest",
    logStreamName: "2023/01/12/[$LATEST]2686118ad1fe4312ac2649853f963de9",
    invokedFunctionArn: "arn:aws:lambda:us-east-1:123456789012:function:ttest",
    awsRequestId: "f1233169-263d-5453-81ea-6d53188b5d98",
  };

  beforeEach(() => {
    messages = [];
    errorMessages = [];
    queueProducer = Mock.ofType<Producer>();
    errorProducer = Mock.ofType<Producer>();
    queueProducer
      .setup(async (producer) => producer.send(It.isAny()))
      .callback((value) => messages.push(...value));
    errorProducer
      .setup(async (producer) => producer.send(It.isAny()))
      .callback((value) => errorMessages.push(...value));
  });

  it("Basic Request", async () => {
    const runner = new SqsRunner(
      service,
      adapter,
      { ErrorQueueUrl: "error", ResponseQueueUrl: "response" },
      errorProducer.object,
      queueProducer.object
    );
    await runner.execute(exampleRequest, exampleContext);
    assert.equal(messages.length, 1);
    assert.equal(errorMessages.length, 0);
    assert.isDefined(messages[0].id);
    assert.deepEqual(JSON.parse(messages[0].body), {
      name: "health_check",
      payload: {
        body: "",
        headers: {},
        statusCode: 204,
      },
    });
  });

  it("Error Request", async () => {
    const runner = new SqsRunner(
      service,
      adapter,
      { ErrorQueueUrl: "error", ResponseQueueUrl: "response" },
      errorProducer.object,
      queueProducer.object
    );
    let isError = false;
    try {
      await runner.execute(willFailRequest, exampleContext);
    } catch (error) {
      isError = true;
    }

    assert.isTrue(isError);
    assert.equal(messages.length, 0);
    assert.equal(errorMessages.length, 0);
  });

  it("Basic Request with fifo", async () => {
    const runner = new SqsRunner(
      service,
      adapter,
      { ErrorQueueUrl: "error.fifo", ResponseQueueUrl: "response.fifo" },
      errorProducer.object,
      queueProducer.object
    );
    await runner.execute(exampleRequest, exampleContext);
    assert.equal(messages.length, 1);
    assert.equal(errorMessages.length, 0);
    assert.isDefined(messages[0].id);
    assert.isDefined(messages[0].groupId);
    assert.isDefined(messages[0].deduplicationId);
    assert.deepEqual(JSON.parse(messages[0].body), {
      name: "health_check",
      payload: {
        body: "",
        headers: {},
        statusCode: 204,
      },
    });
  });
});
