import { Response } from "mangaotaku-configurapi";
import { SQSRecord } from "aws-lambda";
import { assert } from "chai";
import { SqsAdapter } from "../src/entities/sqsAdapter";
import { getEventContext } from "./testHelper";

describe("SqsAdapter", async () => {
  it("Basic Get Event", async () => {
    const { event, context } = getEventContext("awesome_event_name", "./sqsEventContext.json");
    const request = new SqsAdapter().toRequest(event, context);
    assert.equal(request.name, "awesome_event_name");
    assert.deepEqual(request.headers, {
      attributes: {
        ApproximateReceiveCount: "1",
        SentTimestamp: "1673542779372",
        SenderId: "senderId",
        ApproximateFirstReceiveTimestamp: "1673542779382",
      },
      messageAttributes: {},
      approximateReceiveCount: "1",
    } as unknown);
    assert.isUndefined(request.path, "");
    assert.equal(request.method, "");
    assert.deepEqual(request.payload, { key: "value" });
    assert.isUndefined(request.pathAndQuery, "");
  });

  it("Invoke Get Event", async () => {
    const event: SQSRecord = {
      Records: [
        {
          some: "thing",
        },
      ],
    } as unknown as SQSRecord;
    const context = {};
    const request = new SqsAdapter().toRequest(event, context);
    assert.equal(request.name, "");
    assert.deepEqual(request.headers, {
      attributes: undefined,
      messageAttributes: undefined,
      approximateReceiveCount: undefined,
    } as unknown);
    assert.isUndefined(request.path, "");
    assert.equal(request.method, "");
    assert.deepEqual(request.payload, { Records: [{ some: "thing" }] });
    assert.isUndefined(request.pathAndQuery, "");
  });

  it("Basic Response", async () => {
    const response = new SqsAdapter().toResponse(
      new Response({ name: "just_do_it", body: ["value1", "value2"], key3: false }, 200, {
        "content-type": "application/json",
      })
    );

    assert.equal(response.name, "just_do_it");
    assert.deepEqual(response.body, {
      name: "just_do_it",
      body: ["value1", "value2"],
      key3: false,
    });
  });

  it("Basic Response", async () => {
    const response = new SqsAdapter().toResponse(
      new Response({ name: "just_do_it", key2: ["value1", "value2"], key3: false }, 200, {
        "content-type": "application/json",
      })
    );

    assert.equal(response.name, "just_do_it");
    assert.deepEqual(response.body, {
      name: "just_do_it",
      key2: ["value1", "value2"],
      key3: false,
    });
  });
});
