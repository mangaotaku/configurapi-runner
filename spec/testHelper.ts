import { readFileSync } from "fs";

export function getEventContext(eventName: string, fileName: string): { event; context } {
  const eventList = JSON.parse(readFileSync(fileName).toString());
  const data = eventList.find(
    (item: { event: { path: string; rawPath: string; body: string } }) =>
      item.event?.path === eventName ||
      item.event?.rawPath === eventName ||
      item.event?.body?.includes(eventName)
  );
  return {
    event: data.event,
    context: data.context,
  };
}
