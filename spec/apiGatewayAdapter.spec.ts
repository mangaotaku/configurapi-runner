import { Response } from "mangaotaku-configurapi";
import { assert } from "chai";

import { LambdaAdapter } from "../src/entities/lambdaAdapter";
import { getEventContext } from "./testHelper";

describe("ApiGatewayAdapter", async () => {
  it("Basic Get Event", async () => {
    const { event, context } = getEventContext("/something", "./apiGatewayEventContext.json");
    const request = new LambdaAdapter().toRequest(event, context);

    assert.isUndefined(request.name);
    assert.deepEqual(request.headers, { "content-type": "application/json" });
    assert.equal(request.path, "/something");
    assert.equal(request.method, "get");
    assert.isUndefined(request.payload);
    assert.equal(request.pathAndQuery, "/something");
  });

  it("Basic Post Event", async () => {
    const { event, context } = getEventContext("/object", "./apiGatewayEventContext.json");
    const request = new LambdaAdapter().toRequest(event, context);

    assert.isUndefined(request.name);
    assert.deepEqual(request.headers, { "content-type": "application/json" });
    assert.equal(request.path, "/object");
    assert.equal(request.method, "post");
    assert.deepEqual(request.payload, { something: { key: "value" } });
    assert.equal(request.pathAndQuery, "/object");
  });

  it("Basic Response", async () => {
    const response = new LambdaAdapter().toResponse(
      new Response({ key: "value", key1: ["value1", "value2"], key3: false }, 200, {
        "content-type": "application/json",
      })
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(response.headers, { "content-type": "application/json" });
    assert.isUndefined(response.multiValueHeaders);
    assert.deepEqual(response.body, '{"key":"value","key1":["value1","value2"],"key3":false}');
    assert.equal(response.isBase64Encoded, false);
  });

  it("ELB Response", async () => {
    const response = new LambdaAdapter().toResponse(
      {
        ...new Response(
          {
            key: "value",
            key1: new Set(["value1", "value2", "value1"]),
            key3: false,
          },
          200,
          { "content-type": "application/json" }
        ),
        ...{
          jsonReplacer: (key, value) => (value instanceof Set ? [...value] : value),
        },
      },
      { elbProvider: true }
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(response.headers, { "content-type": "application/json" });
    assert.deepEqual(response.multiValueHeaders, {
      "content-type": ["application/json"],
    });
    assert.equal(response.statusDescription, `200 OK`);
    assert.deepEqual(response.body, '{"key":"value","key1":["value1","value2"],"key3":false}');
    assert.equal(response.isBase64Encoded, false);
  });

  it("Basic Response - string body", async () => {
    const response = new LambdaAdapter().toResponse(
      new Response("bananabanana", 200, { "content-type": "plain/text" })
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(response.headers, { "content-type": "plain/text" });
    assert.isUndefined(response.multiValueHeaders);
    assert.equal(response.body, "bananabanana");
    assert.equal(response.isBase64Encoded, false);
  });

  it("Basic Response - number body", async () => {
    const response = new LambdaAdapter().toResponse(
      new Response(1337, 200, { "content-type": "plain/text" })
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(response.headers, { "content-type": "plain/text" });
    assert.isUndefined(response.multiValueHeaders);
    assert.equal(response.body, "1337");
    assert.equal(response.isBase64Encoded, false);
  });

  it("Basic Response - buffer body", async () => {
    const response = new LambdaAdapter().toResponse(
      new Response(Buffer.from("hahahaha"), 200, {
        "content-type": "plain/text",
      })
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(response.headers, { "content-type": "plain/text" });
    assert.isUndefined(response.multiValueHeaders);
    assert.equal(response.body, Buffer.from("hahahaha").toString("base64"));
    assert.equal(response.isBase64Encoded, true);
  });

  it("Basic Response - no body", async () => {
    const response = new LambdaAdapter().toResponse(
      new Response(undefined, 200, { "content-type": "plain/text" })
    );

    assert.equal(response.statusCode, 200);
    assert.deepEqual(response.headers, { "content-type": "plain/text" });
    assert.isUndefined(response.multiValueHeaders);
    assert.isEmpty(response.body, "");
    assert.equal(response.isBase64Encoded, false);
  });
});
