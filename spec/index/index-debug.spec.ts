import { assert } from "chai";

process.env.CONFIG_PATH = "logging-config.yaml";
process.env.CONSOLE_OUT = "true";

describe("Index", () => {
  it("Basic requests with debug log level", async () => {
    process.env.LOG_LEVEL = "debug";
    let logged = 0;
    console.log = (...str) => (logged += 1);
    assert.equal(logged, 0);

    await (
      await require("../../src/index")
    ).handler(
      {
        invoke: true,
        headers: { "Content-Type": "application/json" },
        name: "some_event",
        payload: {
          id: "id",
          key: "value",
        },
      },
      {},
      () => {}
    );
    assert.equal(logged, 9);
  });
});
