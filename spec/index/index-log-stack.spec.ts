import { assert } from "chai";

process.env.CONFIG_PATH = "logging-config.yaml";
process.env.CONSOLE_OUT = "true";
describe("Index", () => {
  it("Basic requests with none log level, expect all logs on error", async () => {
    process.env.LOG_LEVEL = "none";
    let logged = 0;
    console.log = (str) => (logged += 1);
    assert.equal(logged, 0);

    await (
      await require("../../src/index")
    ).handler(
      {
        invoke: "true",
        headers: { "Content-Type": "application/json" },
        name: "exception_event",
        payload: {
          name: "exception_event ",
          key: "value",
        },
      },
      {},
      () => {}
    );
    assert.equal(logged, 11);
  });
});
