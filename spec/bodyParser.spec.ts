import { assert } from "chai";
import { BodyParser } from "../src/entities/bodyParser";

describe("BodyParser", async () => {
  it("parse", async () => {
    assert.equal(BodyParser.parse("banana"), "banana");
    assert.equal(BodyParser.parse(true), "true");
    assert.equal(BodyParser.parse(Buffer.from("banana")), Buffer.from("banana").toString("base64"));
    assert.equal(
      BodyParser.parse(
        {
          key: "value",
          key1: new Set(["value1", "value2", "value1"]),
          key3: false,
        },
        (key, value) => (value instanceof Set ? [...value] : value)
      ),
      '{"key":"value","key1":["value1","value2"],"key3":false}'
    );
    assert.equal(
      BodyParser.parse({
        key: true,
        value: [1, 2, 3],
        value2: { key2: "key3" },
      }),
      '{"key":true,"value":[1,2,3],"value2":{"key2":"key3"}}'
    );
  });
});
