/* eslint-disable no-unmodified-loop-condition */
import { assert } from "chai";

import WebSocket from "ws";
import { ConfigurapiCommand } from "../src/entities/configurapiCommand";
import { HttpRunner } from "../src/entities/httpRunner";
const port = 12345;
const sPort = 12346;

describe("HttpsService", async () => {
  const service: HttpRunner = new HttpRunner(
    new ConfigurapiCommand(
      "",
      "",
      "--key",
      "ca.key",
      "--cert",
      "ca.crt",
      "--port",
      `${port}`,
      "--s-port",
      `${sPort}`
    )
  );
  const sleep = async (ms) => new Promise((request) => setTimeout(request, ms));

  before(() => {
    service.run();
  });

  it("basic request", async () => {
    const request = await fetch(`https://localhost:${sPort}/collections`);
    assert.equal(await request.text(), "Hi there");
    assert.equal(request.status, 201);
  });

  it("Secure WebSocket Test", async () => {
    let asserts = 0;
    let closed = false;
    const socket = new WebSocket(`wss://localhost:${sPort}/collections`);
    socket.on("open", () => {
      socket.send(JSON.stringify({ name: "list_collections" }));
      // Listen for messages
      socket.on("message", (event: Buffer) => {
        console.log("Client received a message", Buffer.from(event).toString());
        assert.equal(Buffer.from(event).toString(), "Hi there");
        asserts += 1;
        socket.close();
      });

      // Listen for socket closes
      socket.on("close", (event) => {
        console.log("Client notified socket has closed", event);
        socket.close();
        socket.terminate();
      });

      socket.on("error", (error) => {
        console.log("error", error);
        socket.close();
        socket.terminate();
      });
    });

    socket.on("close", () => {
      closed = true;
    });

    while (!closed) {
      console.log("waiting");
      await sleep(1000);
    }

    assert.equal(asserts, 1);
  }).timeout(3000);

  after(() => {
    service.stop();
  });
});
