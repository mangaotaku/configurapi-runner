/* eslint-disable no-unmodified-loop-condition */

import { assert } from "chai";
import WebSocket from "ws";
import { ConfigurapiCommand } from "../src/entities/configurapiCommand";
import { HttpRunner } from "../src/entities/httpRunner";

const port = 12347;
describe("HttpService", async () => {
  const service: HttpRunner = new HttpRunner(new ConfigurapiCommand("", "", "--port", `${port}`));
  const sleep = async (ms) => new Promise((request) => setTimeout(request, ms));

  before(() => {
    service.run();
  });

  it("basic request", async () => {
    const request = await fetch(`http://localhost:${port}/collections`);
    assert.equal(await request.text(), "Hi there");
    assert.equal(request.status, 201);
  });

  it("WebSocket Test", async () => {
    let asserts = 0;
    let closed = false;
    const socket = new WebSocket(`ws://localhost:${port}/collections`);
    socket.on("open", () => {
      socket.send(JSON.stringify({ name: "list_collections" }));
      // Listen for messages
      socket.on("message", (event: Buffer) => {
        console.log("Client received a message", Buffer.from(event).toString());
        assert.equal(Buffer.from(event).toString(), "Hi there");
        asserts += 1;
        socket.close();
      });

      // Listen for socket closes
      socket.on("close", (event) => {
        console.log("Client notified socket has closed", event);
        socket.close();
        socket.terminate();
      });

      socket.on("error", (error) => {
        console.log("error", error);
        socket.close();
        socket.terminate();
      });
    });

    socket.on("close", () => {
      closed = true;
    });

    while (!closed) {
      console.log("waiting");
      await sleep(1000);
    }

    assert.equal(asserts, 1);
  }).timeout(3000);

  after(() => {
    service.stop();
  });
});
