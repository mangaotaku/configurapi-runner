import { Config, Service } from "mangaotaku-configurapi";
import { assert } from "chai";

import { LambdaRunner } from "../src/entities/lambdaRunner";
import { getEventContext } from "./testHelper";

describe("LambdaRunner", async () => {
  const service = new Service(Config.load("./lambda-config.yaml"));
  service.on("error", () => {});
  const runner = new LambdaRunner(service);

  [
    {
      name: "Basic Test",
      data: getEventContext("/v1/test/1234", "./lambdaEventContext.json"),
      expectedResponse: {
        statusCode: 200,
        headers: { "Content-Type": "application/json" },
        body: '{"id":"id","key":"value"}',
        isBase64Encoded: false,
      },
    },
    {
      name: "Basic Error Test",
      data: getEventContext("/v1/errors/1234", "./lambdaEventContext.json"),
      expectedResponse: {
        statusCode: 403,
        body: '{"statusCode":403,"message":"Error Response","details":""}',
        headers: { "Content-Type": "application/json" },
        isBase64Encoded: false,
      },
    },
    {
      name: "Basic Endpoint Test",
      data: getEventContext("/v1/endpoint", "./endpointEventContext.json"),
      expectedResponse: {
        statusCode: 200,
        headers: { "Content-Type": "application/json" },
        body: '{"id":"id","key":"value"}',
        isBase64Encoded: false,
      },
    },
    {
      name: "Basic Endpoint Error Test",
      data: getEventContext("/v1/endpointerror", "./endpointEventContext.json"),
      expectedResponse: {
        statusCode: 403,
        body: '{"statusCode":403,"message":"Error Response","details":""}',
        headers: { "Content-Type": "application/json" },
        isBase64Encoded: false,
      },
    },
  ].map(async (test) =>
    it(test.name, async () => {
      assert.deepEqual(
        await runner.execute(test.data.event, test.data.context),
        test.expectedResponse
      );
    })
  );
});
