# configurapi-runner

This project is the base adapter which can run serverless services on aws and local instances as well.

## Project Design

![alt text](Architecture.png)

The runner is responsible for massaging the data from any source into an event which Configurapi expects. This allows us to reuse the same architecture and design accross multiple service implementations.

## Integration

When adding this library to a project, **dist/index.handler must be set as the index handler** for the serverless lambda function.

This library can be used to run an Api/SQS/Lambda service locally as well.
Examples:

```bash
cd /path/to/config.yaml/folder && ts-node ../node_modules/.bin/configurapi-runner-sqs -q http://queueurl:9324/queue/queue_name_request -r http://queueurl:9324/queue/queue_name_response -e http://queueurl:9324/queue/queue_name_error
```

```bash
cd src && ts-node ../node_modules/.bin/configurapi-runner-self -p 1234
```

## Cli Options

- -q, --queueUrl [string], The request queue url
- -r, --ResponseQueueUrl [string], The response queue url
- -e, --ErrorQueueUrl [string], The error queue url
- -t, --type [string], Runner type
- -p, --port [number], Port number
- -s, --self [boolean], Run Self
- --s-port [number], Secure port number
- -f, --file [path], Path to the config file
- --key [path], Path to the private key file (.key)
- --cert [path], Path to the certificate file (.pem)
- -k, --keep-alive [number], The number of milliseconds of inactivity a server needs to wait. Default to 0 which disable the keep alive behavior,
- -c, --on-connect [boolean], Execute connect handler on connect.
- -d, --on-disconnect [boolean], Execute disconnect handler on disconnect.

## Testing

- Mocha tests located under spec/.

To run tests, run `npm test`

## Linting

`npm run lint`
