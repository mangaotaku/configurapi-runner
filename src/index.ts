/* eslint-disable no-unused-expressions */
import { Config, ErrorResponse, LogLevel, Service } from "mangaotaku-configurapi";
import { Callback, Context } from "aws-lambda";
import dotenv from "dotenv";

import { IAwsRequest } from "./interfaces/iAwsRequest";
import { IAwsResponse } from "./interfaces/iAwsResponse";

const logLevel = process.env.LOG_LEVEL;
const useConsoleOut = process.env.CONSOLE_OUT;
const debug = process.env.DEBUG;
let originalLog;

process.env.DOTENV_CONFIG_PATH
  ? dotenv.config({ path: process.env.DOTENV_CONFIG_PATH })
  : dotenv.config();

function printDebugLog(str: string): void {
  if (debug) {
    process.stdout.write(`${str}\n`);
  }
}

function log(level: LogLevel, ...msg: string[]) {
  if (!logLevel || logLevel !== "none" || (logLevel && LogLevel.gte(logLevel as LogLevel, level))) {
    process.stdout.write(`${msg}\n`);
  }
}

const service = new Service(Config.load(process.env.CONFIG_PATH || "./config.yaml"));
service.on(LogLevel.Trace, (...str: string[]) => log(LogLevel.Trace, ...str));
service.on(LogLevel.Info, (...str: string[]) => log(LogLevel.Info, ...str));
service.on(LogLevel.Error, (...str: string[]) => log(LogLevel.Error, ...str));
service.on(LogLevel.Warn, (...str: string[]) => log(LogLevel.Warn, ...str));

// eslint-disable-next-line max-statements
export async function handler(
  awsEvent: IAwsRequest,
  context: Partial<Context>,
  callback: Callback
) {
  printDebugLog(`Received Event: ${JSON.stringify(awsEvent)}, Context: ${JSON.stringify(context)}`);
  const logStack = [];

  await service.loadPlugins;
  /* 
    After loading plugins, console.log will have been overwritten to send the logs to newrelic
    instead of printing them out.

    After the runner is executed, console.log will be overwritten again to emit as service event.
  */
  originalLog = originalLog || console.log;

  const logFn = (level: LogLevel, ...msg: [string | object]) => {
    const message = require("./entities/logFeature").LogFeature.logWithEvent(
      level,
      undefined,
      ...msg
    );
    if (logLevel === undefined) {
      useConsoleOut ? originalLog(message) : process.stdout.write(`${message}\n`);
    } else if (logLevel !== "none" && LogLevel.gte(logLevel as LogLevel, level)) {
      useConsoleOut ? originalLog(message) : process.stdout.write(`${message}\n`);
    } else {
      logStack.push(message);
    }
  };

  service.removeAllListeners();
  service.on(LogLevel.Info, (...str: [string | object]) => logFn(LogLevel.Info, ...str));
  service.on(LogLevel.Debug, (...str: [string | object]) => logFn(LogLevel.Debug, ...str));
  service.on(LogLevel.Error, (...str: [string | object]) => logFn(LogLevel.Error, ...str));
  service.on(LogLevel.Warn, (...str: [string | object]) => logFn(LogLevel.Warn, ...str));
  service.on(LogLevel.Trace, (...str: [string | object]) => logFn(LogLevel.Trace, ...str));

  try {
    let response: IAwsResponse;
    if (awsEvent.invoke) {
      printDebugLog(`Executing invoke event.`);
      response = await new (require("./entities/invokeRunner").InvokeRunner)(service).execute(
        awsEvent,
        context
      );
    } else if (awsEvent.path || awsEvent.rawPath || awsEvent.payload) {
      printDebugLog(`Executing lambda event.`);
      response = await new (require("./entities/lambdaRunner").LambdaRunner)(service).execute(
        awsEvent,
        context
      );
    } else if (awsEvent.Records) {
      printDebugLog(`Executing sqs event.`);
      response = await new (require("./entities/sqsRunner").SqsRunner)(service).execute(
        awsEvent,
        context
      );
    } else {
      throw new Error(`Received unsupported event '${JSON.stringify(awsEvent)}'.`);
    }
    if (response) {
      callback(null, response);
    } else {
      callback(null);
    }
  } catch (e) {
    for (const stack of logStack) {
      useConsoleOut ? originalLog(stack) : process.stdout.write(`${stack}\n`);
    }
    callback(JSON.stringify(new ErrorResponse(e, -1)));
  }
}
