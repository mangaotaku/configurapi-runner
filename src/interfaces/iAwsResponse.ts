import { APIGatewayProxyResult, DynamoDBBatchResponse, SQSBatchResponse } from "aws-lambda";

export type IAwsResponse = Partial<
  APIGatewayProxyResult | SQSBatchResponse | DynamoDBBatchResponse
>;
