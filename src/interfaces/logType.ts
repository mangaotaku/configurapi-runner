export enum LogType {
  Trace = "Trace",
  Debug = "Debug",
  Error = "Error",
  Info = "Info",
  Warn = "Warn",
}
