export enum QueueType {
  Error = "error",
  Response = "response",
}
