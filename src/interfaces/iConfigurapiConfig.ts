export interface IConfigurapiConfig {
  port: number;
  configPath: string;
  keepAliveTimeout: number;
  key: string;
  cert: string;
  sPort: number;
  onConnect: boolean;
  onDisconnect: boolean;
  type: string;
}
