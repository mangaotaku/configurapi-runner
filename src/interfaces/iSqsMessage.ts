export interface ISqsMessageBody {
  name: string;
  body: object;
  headers: { [key: string]: string };
  statusCode: number;
}
export interface ISqsMessage {
  name: string;
  payload: ISqsMessageBody;
}
