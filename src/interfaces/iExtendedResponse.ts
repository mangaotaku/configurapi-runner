import { IResponse } from "mangaotaku-configurapi";

export interface IExtendedResponse extends IResponse {
  jsonReplacer?: (this: unknown, key: string, value: unknown) => unknown;
}
