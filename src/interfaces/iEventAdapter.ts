import { IRequest } from "mangaotaku-configurapi";
import { Context } from "aws-lambda";

import { IAwsRequest } from "./iAwsRequest";
import { IAwsResponse } from "./iAwsResponse";
import { IExtendedResponse } from "./iExtendedResponse";

export interface IEventAdapter<T = IAwsResponse> {
  toResponse(response: IExtendedResponse, context?: Partial<Context>): T;

  toRequest(awsRequest: IAwsRequest, context?: Partial<Context>): IRequest;
}
