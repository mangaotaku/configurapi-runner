import {
  ALBEvent,
  APIGatewayEventRequestContextV2,
  APIGatewayProxyEventBase,
  APIGatewayProxyEventV2WithRequestContext,
  APIGatewayRequestAuthorizerEvent,
  Context,
  SQSEvent,
  SQSRecord,
} from "aws-lambda";
import { IInvokeRequest } from "./iInvokeRequets";

export type IAwsRequest = Partial<
  SQSEvent &
    APIGatewayProxyEventBase<Context> &
    APIGatewayProxyEventBase<APIGatewayEventRequestContextV2> &
    ALBEvent &
    SQSRecord &
    APIGatewayRequestAuthorizerEvent &
    APIGatewayProxyEventV2WithRequestContext<Context> & { payload: object | string } & {
      requestContext: { http: { path: string; method: string } };
    } & IInvokeRequest
>;
