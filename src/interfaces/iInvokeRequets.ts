export interface IInvokeRequest {
  invoke: boolean;
  name: string;
  payload: { [key: string]: unknown };
  headers?: { [key: string]: string };
  query?: { [key: string]: string };
  method?: string;
}
