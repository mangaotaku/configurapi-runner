import {
  ALBEvent,
  APIGatewayProxyEventBase,
  APIGatewayProxyEventV2WithRequestContext,
  Context,
  SQSEvent,
} from "aws-lambda";

import { IAwsResponse } from "./iAwsResponse";

export interface IEventExecutor {
  execute(
    awsEvent: Partial<
      | SQSEvent
      | APIGatewayProxyEventBase<Context>
      | ALBEvent
      | APIGatewayProxyEventV2WithRequestContext<Context>
    >,
    context: Context
  ): Promise<IAwsResponse | void>;
}
