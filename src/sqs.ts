/* eslint-disable no-unused-expressions */
import dotenv from "dotenv";
process.env.DOTENV_CONFIG_PATH
  ? dotenv.config({ path: process.env.DOTENV_CONFIG_PATH })
  : dotenv.config();

import { ConfigurapiCommand } from "./entities/configurapiCommand";
import { LocalSQSRunner } from "./entities/localSqsRunner";

new LocalSQSRunner(ConfigurapiCommand.fromProcess()).run();
