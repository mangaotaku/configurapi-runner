/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable max-lines-per-function */
/* eslint-disable no-unused-expressions */
import { Config, ErrorResponse, Event, Request, Service } from "mangaotaku-configurapi";
import { IConfig } from "mangaotaku-configurapi/dist/interfaces/iConfig";
import { readFileSync } from "fs";
import { IncomingMessage, ServerResponse } from "http";
import { v4 } from "uuid";
import { RawData, Server, WebSocket } from "ws";

import { IConfigurapiConfig } from "../interfaces/iConfigurapiConfig";
import { HttpAdapter } from "./httpAdapter";

class WebsocketResponse extends ServerResponse {
  data = "";
  constructor(req: IncomingMessage, onEnd: (args) => any) {
    super(req);
    this.end = onEnd;
  }
}

interface IHttpRunner extends IConfig {
  port?: number;
  sPort?: number;
  configPath: string;
  service: Service;
  keepAliveTimeout: number;
  key: string;
  cert: string;
  connections: object;
  onConnect: boolean;
  onDisconnect: boolean;
}

require("http-shutdown").extend();

export class HttpRunner extends Service {
  connections: object;
  server: any;
  httpsServer: any;
  wssServer: any;

  constructor(private readonly serviceConfig: Partial<IConfigurapiConfig>) {
    super(Config.load(serviceConfig.configPath));
    this.connections = {};
  }

  run() {
    this.on("error", (str) => console.log(str));
    this.on("debug", (str) => console.log(str));
    this.on("trace", (str) => console.log(str));

    if (this.serviceConfig.cert && this.serviceConfig.key && this.serviceConfig.sPort) {
      this.httpsServer = require("https")
        .createServer(
          {
            key: readFileSync(this.serviceConfig.key),
            cert: readFileSync(this.serviceConfig.cert),
          },
          (req, resp) => {
            this.requestListener(req, resp);
          }
        )
        .withShutdown();

      const wssSecure = new Server({ server: this.httpsServer });

      wssSecure.on("listening", () => console.log("Secure Websocket listening...."));
      wssSecure.on("connection", (ws: WebSocket & { id: string }, wsRequest: IncomingMessage) => {
        ws.id = v4();
        this.connections[ws.id] = ws;
        if (this.serviceConfig.onConnect) {
          const newRequest = new Request("");
          newRequest.name = "connect";
          newRequest.headers = wsRequest.headers as any;
          this.requestListener(
            newRequest as any,
            new WebsocketResponse(newRequest as any, (requestResponse) => {
              if (requestResponse instanceof ErrorResponse) {
                ws.send(requestResponse as any, { binary: false });
                ws.close(requestResponse.statusCode);
              }
            })
          );
        }

        ws.on("message", (req: RawData, isBinary: boolean) => {
          const re = JSON.parse(req.toString());
          re.id = v4();
          this.requestListener(
            re,
            new WebsocketResponse(re, (x) => {
              ws.send(x, { binary: false });
            })
          );
        });

        ws.on("close", (socket: WebSocket & { name: string }, code: number, reason: Buffer) => {
          if (this.serviceConfig.onDisconnect) {
            socket.name = "disconnect";
            this.requestListener(
              socket as any,
              new WebsocketResponse(undefined, (x) => {
                socket.send(x, { binary: false });
              })
            );
          }
        });

        ws.on("error", (error) => {
          console.log("Some Error occurred", error);
        });
      });
      this.httpsServer.keepAliveTimeout = this.serviceConfig.keepAliveTimeout;
      this.httpsServer.listen(this.serviceConfig.sPort);

      this.emit("trace", "Secure Server is listening...");
    }

    this.server = require("http")
      .createServer((req, resp) => {
        this.requestListener(req, resp);
      })
      .withShutdown();

    this.server.keepAliveTimeout = this.serviceConfig.keepAliveTimeout;
    const wss = new Server({ server: this.server });

    wss.on("listening", () => console.log("Websocket listening...."));
    wss.on("connection", (ws: WebSocket & { id: string }, wsRequest: IncomingMessage) => {
      ws.id = v4();
      this.connections[ws.id] = ws;
      if (this.serviceConfig.onConnect) {
        const newRequest = new Request("");
        newRequest.name = "connect";
        newRequest.headers = wsRequest.headers as any;
        this.requestListener(
          newRequest as any,
          new WebsocketResponse(newRequest as any, (requestResponse) => {
            if (requestResponse instanceof ErrorResponse) {
              ws.send(requestResponse as any, { binary: false });
              ws.close(requestResponse.statusCode);
            }
          })
        );
      }

      ws.on("message", (req: RawData, isBinary: boolean) => {
        const re = JSON.parse(req.toString());
        re.id = v4();
        this.requestListener(
          re,
          new WebsocketResponse(re, (x) => {
            ws.send(x, { binary: false });
          })
        );
      });

      ws.on("close", (socket: WebSocket & { name: string }, code: number, reason: Buffer) => {
        if (this.serviceConfig.onDisconnect) {
          socket.name = "disconnect";
          this.requestListener(
            socket as any,
            new WebsocketResponse(undefined, (x) => {
              socket.send(x, { binary: false });
            })
          );
        }
      });

      ws.on("error", (error) => {
        console.log("Some Error occurred", error);
      });
    });

    this.server.listen(this.serviceConfig.port);
    this.emit("trace", "Server is listening...");
  }

  async requestListener(
    incomingMessage: Partial<IncomingMessage & Request>,
    serverResponse: ServerResponse
  ) {
    try {
      const event = new Event(
        (await HttpAdapter.toRequest(incomingMessage)) as Request,
        "x-provenance-id",
        "x-provenance-id"
      );
      event.id = event.id || v4();

      await super.process(event);

      HttpAdapter.write(event.response, serverResponse);
    } catch (error) {
      const response = new ErrorResponse(error, error instanceof SyntaxError ? 400 : 500);

      HttpAdapter.write(response, serverResponse);
    }
  }

  stop() {
    this.server.shutdown();
    this.httpsServer ? this.httpsServer.shutdown() : undefined;
  }
}
