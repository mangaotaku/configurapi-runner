import { IRequest } from "mangaotaku-configurapi";
import { Context, SQSRecord } from "aws-lambda";

import { Config } from "../config";
import { IEventAdapter } from "../interfaces/iEventAdapter";
import { IExtendedResponse } from "../interfaces/iExtendedResponse";
import { ISqsMessageBody } from "../interfaces/iSqsMessage";
import { RunnerRequest } from "./runnerRequest";

export class SqsAdapter implements IEventAdapter<ISqsMessageBody> {
  toResponse(response: Partial<IExtendedResponse>, context?: Partial<Context>): ISqsMessageBody {
    const payload = response.body as ISqsMessageBody;
    const headers = response.body === "" ? {} : { "Content-Type": "application/json" };

    return {
      name: payload.name,
      body: payload,
      statusCode: response.statusCode ?? payload.statusCode ?? 204,
      headers,
    } as ISqsMessageBody;
  }

  toRequest(awsRequest: SQSRecord, context?: Partial<Context>): IRequest {
    const data = awsRequest.body ? JSON.parse(awsRequest.body) : awsRequest;
    return new RunnerRequest({
      method: "",
      headers: {
        ...{
          approximateReceiveCount: awsRequest.attributes?.ApproximateReceiveCount,
          messageAttributes: awsRequest.messageAttributes,
          attributes: awsRequest.attributes,
        },
        ...data.headers,
      },
      name: data.name || Config.DefaultEventName,
      query: data.query || {},
      payload: data.payload || data,
    });
  }
}
