import { Command, OptionValues } from "commander";

import { Config } from "../config";
import { IConfigurapiConfig } from "../interfaces/iConfigurapiConfig";

export class ConfigurapiCommand implements IConfigurapiConfig {
  command: Command;
  options: OptionValues;

  constructor(...args: string[]) {
    this.command = new Command()
      .option("-q, --queueUrl [string]", "The request queue url")
      .option("-r, --ResponseQueueUrl [string]", "The response queue url")
      .option("-e, --ErrorQueueUrl [string]", "The error queue url")
      .option("-t, --type [string]", "Runner type")
      .option("-p, --port [number]", "Port number")
      .option("-s, --self [boolean]", "Run Self")
      .option("--s-port [number]", "Secure port number")
      .option("-f, --file [path]", "Path to the config file")
      .option("--key [path]", "Path to the private key file (.key)")
      .option("--cert [path]", "Path to the certificate file (.pem)")
      .option(
        "-k, --keep-alive [number]",
        "The number of milliseconds of inactivity a server needs to wait. Default to 0 which disable the keep alive behavior"
      )
      .option("-c, --on-connect [boolean]", "Execute connect handler on connect.")
      .option("-d, --on-disconnect [boolean]", "Execute disconnect handler on disconnect.")
      .parse(args);
    this.options = this.command.opts();
  }

  get port(): number {
    return this.options.port || Config.Port;
  }

  get sPort(): number {
    return this.options.sPort || Config.SecurePort;
  }

  get configPath(): string {
    return this.options.configPath || Config.ConfigPath;
  }

  get keepAliveTimeout(): number {
    return this.options.keepAliveTimeout || Config.KeepAliveTimeout;
  }

  get key(): string {
    return this.options.key ? this.options.key : Config.Key;
  }

  get cert(): string {
    return this.options.cert ? this.options.cert : Config.Cert;
  }

  get onConnect(): boolean {
    return this.options.onConnect || Config.OnConnect;
  }

  get onDisconnect(): boolean {
    return this.options.onDisconnect || Config.OnDisconnect;
  }

  get type(): string {
    return this.options.type || "http";
  }

  get requestQueueUrl(): string {
    return this.options.queueUrl || Config.RequestQueueUrl;
  }

  get region(): string {
    return this.options.Region || Config.Region;
  }

  // eslint-disable-next-line @typescript-eslint/naming-convention
  get ResponseQueueUrl(): string {
    return this.options.ResponseQueueUrl || Config.ResponseQueueUrl;
  }

  // eslint-disable-next-line @typescript-eslint/naming-convention
  get ErrorQueueUrl(): string {
    return this.options.ErrorQueueUrl || Config.ErrorQueueUrl;
  }

  get concurrency(): string {
    return this.options.concurrency || Config.ConcurrencyCount;
  }

  static fromProcess(): IConfigurapiConfig {
    return new ConfigurapiCommand(...process.argv);
  }
}
