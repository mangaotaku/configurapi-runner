import { IEvent, IService, LogLevel } from "mangaotaku-configurapi";

import { LogType } from "../interfaces/logType";

const debug = process.env.DEBUG;
export class LogFeature {
  static logDebugEvent = (str: string): void => {
    if (debug) {
      process.stdout.write(`${str}\n`);
    }
  };

  static reduce = (
    acc: Partial<{ [key: string]: string | object | number | boolean }>,
    obj: (object & { message?: string }) | string
  ) => {
    try {
      obj = JSON.parse(obj.toString()); // eslint-disable-line
    } catch (e) {} // eslint-disable-line
    if (typeof obj === "string") {
      if (acc?.message && typeof acc?.message === "string" && acc?.message?.length > 0) {
        acc.message = `${acc?.message} ${obj}`;
      } else {
        acc.message = obj;
      }
      return acc;
    } else {
      /* 
        This is so that we combine messages if there are two objects 
        which both have message in the log stack
      */
      if (obj?.message) {
        obj.message = `${acc?.message} ${obj?.message}`;
      }
      return { ...acc, ...obj };
    }
  };

  static logWithEvent = (logLevel: LogType, event: IEvent, ...messages: string[]) =>
    JSON.stringify(
      messages.reduce(this.reduce, {
        message: "",
        tag: event?.id,
        level: logLevel.toUpperCase(),
        timestamp: new Date().getTime(),
        provenanceId: event?.correlationId,
        container_name: process.env.AWS_LAMBDA_FUNCTION_NAME || "unknown",
      })
    );

  static initialize(service: IService, event: IEvent) {
    console.log = (...message) =>
      service.emit(LogLevel.Info, this.logWithEvent(LogType.Info, event, ...message));
    console.error = (...message) =>
      service.emit(LogLevel.Error, this.logWithEvent(LogType.Error, event, ...message));
    console.warn = (...message) =>
      service.emit(LogLevel.Warn, this.logWithEvent(LogType.Warn, event, ...message));
    console.debug = (...message) =>
      service.emit(LogLevel.Debug, this.logWithEvent(LogType.Debug, event, ...message));
    console.trace = (...message) =>
      service.emit(LogLevel.Trace, this.logWithEvent(LogType.Trace, event, ...message));
  }
}
