import { ErrorResponse, Event, IEvent, Service } from "mangaotaku-configurapi";
import { Context, SQSRecord } from "aws-lambda";
import { Message, Producer } from "sqs-producer";
import { v4 } from "uuid";

import { Config } from "../config";
import { IAwsRequest } from "../interfaces/iAwsRequest";
import { IEventExecutor } from "../interfaces/iEventRunner";
import { QueueType } from "../interfaces/queueType";
import { validateIsFifo } from "./helperFunctions";
import { LogFeature } from "./logFeature";
import { SqsAdapter } from "./sqsAdapter";

export class SqsRunner implements IEventExecutor {
  counter = 0;

  constructor(
    public service: Service,
    private sqsAdapter = new SqsAdapter(),
    private config: Partial<typeof Config> = Config,
    private errorQueue = config.ErrorQueueUrl
      ? Producer.create({
          queueUrl: config.ErrorQueueUrl,
          region: config.Region,
          batchSize: config.ConcurrencyCount,
        })
      : undefined,
    private queue = config.ResponseQueueUrl
      ? Producer.create({
          queueUrl: config.ResponseQueueUrl,
          region: config.Region,
          batchSize: config.ConcurrencyCount,
        })
      : undefined
  ) {}

  async enqueue(queueType: QueueType, data: object, fifo = false): Promise<void> {
    const queue = queueType === QueueType.Response ? this.queue : this.errorQueue;

    const msg: Message = {
      id: v4(),
      body: JSON.stringify(data),
      groupId: fifo ? v4() : undefined,
      deduplicationId: fifo ? v4() : undefined,
    };

    await queue.send([msg]);
  }

  async write(event: Partial<IEvent>, context?: Partial<Context>): Promise<void> {
    if (this.config.ErrorQueueUrl && event.response instanceof ErrorResponse) {
      await this.enqueue(
        QueueType.Error,
        {
          name: event.name,
          payload: this.sqsAdapter.toResponse(event.response),
        },
        validateIsFifo(this.config.ErrorQueueUrl)
      );
      return;
    }

    if (this.config.ResponseQueueUrl) {
      await this.enqueue(
        QueueType.Response,
        {
          name: event.name,
          payload: this.sqsAdapter.toResponse(event.response, context),
        },
        validateIsFifo(this.config.ResponseQueueUrl)
      );
    }
  }

  async processMessage(incomingMessage: SQSRecord, context?: Partial<Context>): Promise<void> {
    try {
      const event = new Event(
        this.sqsAdapter.toRequest(incomingMessage, context),
        Config.CorrelationHeader,
        Config.CorrelationHeader
      );
      event.id = context.awsRequestId;
      LogFeature.initialize(this.service, event);
      await this.service.process(event);
      await this.write(event, context);
    } catch (error) {
      console.error(`Error thrown when processing message: ${error.message}`);
      throw error;
    }
  }

  async execute(awsEvent: IAwsRequest, context?: Partial<Context>): Promise<void> {
    await Promise.all(awsEvent.Records.map(async (record) => this.processMessage(record, context)));
  }
}
