/* eslint-disable default-param-last */

export class BodyParser {
  static parse(
    body: unknown = "",
    jsonReplacer?: (this: unknown, key: string, value: unknown) => unknown
  ): string {
    if (body instanceof Buffer) {
      return body.toString("base64");
    } else if (body instanceof Object || typeof body === "object") {
      return JSON.stringify(body, jsonReplacer);
    } else {
      return `${body}`;
    }
  }
}
