import { IRequest, Request } from "mangaotaku-configurapi";

export class RunnerRequest extends Request implements IRequest {
  constructor(request?: Partial<IRequest>) {
    super(request.method);

    this.name = request?.name;
    this.method = request?.method;
    this.headers = request?.headers;
    this.params = request?.params;
    this.payload = request?.payload;
    this.query = request?.query;
    this.path = request?.path;
    this.pathAndQuery = request?.pathAndQuery;
  }
}
