import { Event, Service } from "mangaotaku-configurapi";
import { Context } from "aws-lambda";

import { Config } from "../config";
import { IAwsRequest } from "../interfaces/iAwsRequest";
import { IAwsResponse } from "../interfaces/iAwsResponse";
import { IEventExecutor } from "../interfaces/iEventRunner";
import { EndpointAdapter } from "./endpointAdapter";
import { LambdaAdapter } from "./lambdaAdapter";
import { LogFeature } from "./logFeature";

export class LambdaRunner implements IEventExecutor {
  constructor(
    public service: Service,
    private lambdaAdapter = new LambdaAdapter(),
    private endpointAdapter = new EndpointAdapter()
  ) {}

  async execute(awsEvent: IAwsRequest, context: Context): Promise<IAwsResponse> {
    const event = awsEvent.rawPath
      ? new Event(
          this.endpointAdapter.toRequest(awsEvent, context),
          Config.CorrelationHeader,
          Config.CorrelationHeader
        )
      : new Event(
          this.lambdaAdapter.toRequest(awsEvent, context),
          Config.CorrelationHeader,
          Config.CorrelationHeader
        );
    event.id = context.awsRequestId;
    LogFeature.initialize(this.service, event);
    await this.service.process(event);
    return this.lambdaAdapter.toResponse(event.response, context);
  }
}
