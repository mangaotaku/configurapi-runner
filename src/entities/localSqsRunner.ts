/* eslint-disable @typescript-eslint/naming-convention */
import { SQSClient } from "@aws-sdk/client-sqs";
import {
  Config as ConfigurapiConfig,
  ErrorResponse,
  Event,
  IEvent,
  Service,
} from "mangaotaku-configurapi";
import { Context, SQSRecord } from "aws-lambda";
import { Consumer, ConsumerOptions } from "sqs-consumer";
import { Message, Producer } from "sqs-producer";
import { v4 } from "uuid";
import { Config } from "../config";
import { QueueType } from "../interfaces/queueType";
import { validateIsFifo } from "./helperFunctions";
import { LogFeature } from "./logFeature";
import { NoQueueError } from "./noQueueError";
import { SqsAdapter } from "./sqsAdapter";

export interface ILocalSqsRunnerOptions {
  isFifo: boolean;
  ErrorQueueUrl: string;
  ResponseQueueUrl: string;
  requestQueueUrl: string;
  ConcurrencyCount: number;
  configPath: string;
  region: string;
}

export class LocalSQSRunner extends Service {
  counter: number;
  consumer: Consumer;
  isFifo: boolean;
  ErrorQueueUrl: string;
  ResponseQueueUrl: string;
  ConcurrencyCount: number;
  requestQueueUrl: string;
  configPath: string;
  region: string;

  constructor(
    options: Partial<ILocalSqsRunnerOptions>,
    private sqsAdapter = new SqsAdapter(),
    private errorQueue = Producer.create({
      queueUrl: options.ErrorQueueUrl,
      region: options.region || Config.Region,
      batchSize: options.ConcurrencyCount,
      sqs: new SQSClient({
        endpoint: options.ErrorQueueUrl,
        region: options.region || Config.Region,
      }),
    }),
    private queue = Producer.create({
      queueUrl: options.ResponseQueueUrl,
      region: options.region || Config.Region,
      batchSize: options.ConcurrencyCount,
      sqs: new SQSClient({
        endpoint: options.ResponseQueueUrl,
        region: options.region || Config.Region,
      }),
    })
  ) {
    super(ConfigurapiConfig.load(options.configPath));
    this.isFifo = options?.isFifo;
    this.requestQueueUrl = options?.requestQueueUrl;
    this.ErrorQueueUrl = options?.ErrorQueueUrl;
    this.ResponseQueueUrl = options?.ResponseQueueUrl;
    this.ConcurrencyCount = options?.ConcurrencyCount;
    this.configPath = options?.configPath;
    this.region = options?.region || Config.Region;
  }

  async run() {
    this.on("trace", (str) => console.log("trace", str));
    this.on("debug", (str) => console.log("debug", str));
    this.on("error", (str) => console.log("error", str));
    this.consumer = Consumer.create({
      queueUrl: this.requestQueueUrl,
      batchSize: this.ConcurrencyCount,
      handleMessage: async (message: SQSRecord) => {
        this.counter += 1;
        try {
          await this.requestListener(message);
        } finally {
          this.counter -= 1;
        }
      },
      sqs: new SQSClient({
        endpoint: this.requestQueueUrl,
        region: this.region,
      }),
    } as ConsumerOptions);

    this.consumer.on("error", (err) => this.emit("error", err.message));

    this.consumer.start();

    this.emit("trace", "Server is listening...");
  }

  async write(event: Partial<IEvent>, context?: Partial<Context>) {
    if (event.response instanceof ErrorResponse) {
      if (this.ErrorQueueUrl) {
        await this.enqueue(
          QueueType.Error,
          {
            name: event.name,
            payload: this.sqsAdapter.toResponse(event.response),
          },
          validateIsFifo(this.ErrorQueueUrl)
        );
        return;
      } else {
        throw new NoQueueError();
      }
    }

    if (this.ResponseQueueUrl) {
      await this.enqueue(
        QueueType.Response,
        {
          name: event.name,
          payload: this.sqsAdapter.toResponse(event.response, context),
        },
        validateIsFifo(this.ResponseQueueUrl)
      );
    }
  }

  async enqueue(queueType: QueueType, data: object, isFifo = false): Promise<void> {
    const queue = queueType === QueueType.Response ? this.queue : this.errorQueue;

    const msg: Message = {
      id: v4(),
      body: JSON.stringify(data),
      groupId: isFifo ? v4() : undefined,
      deduplicationId: isFifo ? v4() : undefined,
    };

    queue.send([msg]);
  }

  async stop() {
    if (this.consumer) {
      this.consumer.stop({});

      const sleep = async (ms: number) =>
        new Promise((resolve2, reject2) => setTimeout(resolve2, ms));

      while (this.counter > 0) {
        this.emit("trace", `Server is stopping...[${this.counter}]`);
        await sleep(500);
      }

      this.emit("trace", "Server is stopped.");
    }
  }

  async requestListener(incomingMessage: SQSRecord, context?: Partial<Context>) {
    try {
      const translatedMsg: SQSRecord = {} as SQSRecord;
      for (const key of Object.keys(incomingMessage)) {
        translatedMsg[`${key.charAt(0).toLocaleLowerCase() + key.slice(1)}`] = incomingMessage[key];
      }
      const event = new Event(this.sqsAdapter.toRequest(translatedMsg));
      LogFeature.initialize(this, event);

      await this.process(event);
      await this.write(event, context);
    } catch (error) {
      const response = new ErrorResponse(error, error instanceof SyntaxError ? 400 : 500);

      try {
        await this.write({ name: "error", response }, context);
      } catch (err) {
        try {
          this.emit("error", JSON.stringify(err));
        } catch (errorFromListner) {
          console.log(errorFromListner);
        }
      }
    }
  }
}
