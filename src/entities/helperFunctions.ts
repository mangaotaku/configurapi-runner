export function stringify(...strings: string[]): string {
  return strings
    .map((strand) => (typeof strand === "string" ? strand : JSON.stringify(strand)))
    .join(" ")
    .replace(/[\r\n\t ]+/gu, " ");
}
const FIFO_POSTFIX = ".fifo";

export function validateIsFifo(queueUrl: string) {
  return queueUrl.endsWith(FIFO_POSTFIX);
}
