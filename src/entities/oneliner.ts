export function oneliner(...values: string[]): string {
  return values.map((value: string) => value.replace(/[\r\n\t ]+/g, " ")).join(" ");
}
