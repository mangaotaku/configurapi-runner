import { Event, Service } from "mangaotaku-configurapi";
import { Context } from "aws-lambda";

import { Config } from "../config";
import { IAwsResponse } from "../interfaces/iAwsResponse";
import { IEventExecutor } from "../interfaces/iEventRunner";
import { IInvokeRequest } from "../interfaces/iInvokeRequets";
import { InvokeAdapter } from "./invokeAdapter";
import { LogFeature } from "./logFeature";

export class InvokeRunner implements IEventExecutor {
  constructor(public service: Service, private invokeAdapter = new InvokeAdapter()) {}

  async execute(
    incomingMessage: IInvokeRequest,
    context?: Partial<Context>
  ): Promise<IAwsResponse> {
    const event = new Event(
      this.invokeAdapter.toRequest(incomingMessage, context),
      Config.CorrelationHeader,
      Config.CorrelationHeader
    );
    event.id = context.awsRequestId;
    LogFeature.initialize(this.service, event);
    await this.service.process(event);
    return this.invokeAdapter.toResponse(event.response, context);
  }
}
