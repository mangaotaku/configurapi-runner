import { IRequest, Request } from "mangaotaku-configurapi";
import { APIGatewayProxyResult, Context } from "aws-lambda";
import status = require("http-status");

import { ContentType } from "../interfaces/contentType";
import { IAwsRequest } from "../interfaces/iAwsRequest";
import { IEventAdapter } from "../interfaces/iEventAdapter";
import { IExtendedResponse } from "../interfaces/iExtendedResponse";
import { BodyParser } from "./bodyParser";

export class EndpointAdapter
  implements IEventAdapter<Partial<APIGatewayProxyResult & { statusDescription: string }>>
{
  toResponse(
    response: Partial<IExtendedResponse>,
    context?: Partial<Context & { elbProvider: boolean }>
  ): Partial<APIGatewayProxyResult & { statusDescription: string }> {
    if (response.headers?.awsCallbackWaitsForEmptyEventLoop) {
      context.callbackWaitsForEmptyEventLoop = !!response.headers.awsCallbackWaitsForEmptyEventLoop;
    }

    if (context?.elbProvider) {
      return {
        statusCode: response.statusCode,
        headers: response.headers,
        body: BodyParser.parse(response.body, response?.jsonReplacer),
        statusDescription: `${response.statusCode} ${status[response.statusCode]}`,
        isBase64Encoded: response.body instanceof Buffer,
        multiValueHeaders: Object.keys(response.headers).reduce((memo, key) => {
          memo[key] = [response.headers[key]];
          return memo;
        }, {}),
      };
    }
    return {
      statusCode: response.statusCode,
      headers: response.headers,
      body: BodyParser.parse(response.body, response?.jsonReplacer),
      isBase64Encoded: response.body instanceof Buffer,
    };
  }

  toRequest(awsRequest: IAwsRequest, context?: Partial<Context>): IRequest {
    const request = new Request(
      (awsRequest.httpMethod || awsRequest?.requestContext?.http?.method || "").toLowerCase()
    );

    for (const headerKey of Object.keys(awsRequest.headers || {})) {
      request.headers[headerKey.toLowerCase()] = awsRequest.headers[headerKey];
    }

    request.payload = awsRequest.isBase64Encoded
      ? Buffer.from(awsRequest.body, "base64").toString()
      : awsRequest.body || undefined;

    request.query = {};

    for (const query of awsRequest.rawQueryString?.split("&") || []) {
      const [key, value] = query.split("=");
      request.query[key?.toLowerCase()] = value?.toLowerCase();
    }

    request.query = awsRequest.queryStringParameters
      ? { ...request.query, ...awsRequest.queryStringParameters }
      : request.query;

    for (const queryKey of Object.keys(awsRequest.multiValueQueryStringParameters || {})) {
      const value =
        awsRequest.multiValueQueryStringParameters[queryKey].length === 1
          ? awsRequest.multiValueQueryStringParameters[queryKey][0]
          : awsRequest.multiValueQueryStringParameters[queryKey];

      request.query[queryKey.toLowerCase()] = value;
    }

    request.path = awsRequest?.requestContext?.http?.path || awsRequest?.rawPath || "";
    request.pathAndQuery = [
      awsRequest?.requestContext?.http?.path || awsRequest?.rawPath || "",
      awsRequest?.rawQueryString ? `?${awsRequest.rawQueryString}` : "",
    ].join("");

    if (request.headers["content-type"] === ContentType.ApplicationJson && request.payload) {
      request.payload = JSON.parse(request.payload);
    }

    return request;
  }
}
