import { IRequest } from "mangaotaku-configurapi";
import { APIGatewayProxyResult, Context } from "aws-lambda";

import { Config } from "../config";
import { IEventAdapter } from "../interfaces/iEventAdapter";
import { IExtendedResponse } from "../interfaces/iExtendedResponse";
import { IInvokeRequest } from "../interfaces/iInvokeRequets";
import { BodyParser } from "./bodyParser";
import { RunnerRequest } from "./runnerRequest";

export class InvokeAdapter
  implements IEventAdapter<Partial<APIGatewayProxyResult & { statusDescription: string }>>
{
  toResponse(
    response: Partial<IExtendedResponse>,
    context?: Partial<Context>
  ): Partial<APIGatewayProxyResult & { statusDescription: string }> {
    if (response.headers?.awsCallbackWaitsForEmptyEventLoop) {
      context.callbackWaitsForEmptyEventLoop = !!response.headers.awsCallbackWaitsForEmptyEventLoop;
    }
    return {
      statusCode: response.statusCode,
      headers: response.headers,
      body: BodyParser.parse(response.body, response?.jsonReplacer),
      isBase64Encoded: response.body instanceof Buffer,
    };
  }

  toRequest(invokeRequest: IInvokeRequest, context?: Partial<Context>): IRequest {
    return new RunnerRequest({
      method: invokeRequest.method || "",
      headers: invokeRequest.headers,
      name: invokeRequest.name || Config.DefaultEventName,
      query: invokeRequest.query,
      payload: invokeRequest.payload,
    });
  }
}
