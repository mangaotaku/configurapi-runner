export class NoQueueError extends Error {
  constructor() {
    super("No Queue Available");
  }
}
