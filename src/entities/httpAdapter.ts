import { Request, Response } from "mangaotaku-configurapi";
import { IncomingMessage, ServerResponse } from "http";
import { parse } from "url";
import { v4 } from "uuid";

export class HttpAdapter {
  static write(
    response: Partial<
      Response & { jsonReplacer: (this: unknown, key: string, value: unknown) => unknown }
    >,
    serverResponse: ServerResponse
  ) {
    serverResponse.writeHead(response.statusCode, response.headers);

    let body: Buffer | string;

    if (response.body instanceof Buffer) {
      body = { ...response.body };
    } else if (response.body instanceof String) {
      body = response.body as string;
    } else if (response.body instanceof Object) {
      body = JSON.stringify(response.body, response.jsonReplacer);
    } else if (response.body instanceof Number) {
      body = `${response.body}`;
    } else {
      body = response.body as string;
    }
    serverResponse.end(body || "");
  }

  static async toRequest(incomingMessage: Partial<IncomingMessage & Request>) {
    const request = new Request("");

    request.method = incomingMessage.method?.toLowerCase() || "";
    request.headers = incomingMessage.headers;

    if (incomingMessage.url) {
      const url = parse(incomingMessage.url, true);
      request.payload = undefined;
      request.query = url.query;
      request.path = url.pathname;
      request.pathAndQuery = url.href;
    } else {
      request.headers = incomingMessage.headers || {};
      request.headers["correlation-id"] = request.headers["correlation-id"] || v4();
      request.payload = incomingMessage.payload;
      request.query = incomingMessage.query || {};
      request.name = incomingMessage.name;
      request.path = "";
      request.pathAndQuery = "";
      return request;
    }

    const data: Uint8Array[] = [];
    return new Promise((resolve, reject) => {
      incomingMessage
        .on("data", (chunk) => {
          data.push(chunk);
        })
        .on("end", () => {
          if (data.length <= 0) {
            request.payload = undefined;
            resolve(request);
            return;
          }

          request.payload = Buffer.concat(data).toString();

          if (request.payload && "content-type" in request.headers) {
            const contentType: string = request.headers["content-type"];

            if (contentType.startsWith("text/")) {
              request.payload = request.payload.toString();
            } else if (contentType.startsWith("application/json")) {
              try {
                if (request.payload) {
                  request.payload = JSON.parse(request.payload);
                }
              } catch (err) {
                reject(err);
              }
            }
          }

          resolve(request);
        })
        .on("error", (err) => {
          reject(err);
        });
    });
  }
}
