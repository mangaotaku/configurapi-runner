/* eslint-disable @typescript-eslint/naming-convention */

export class Config {
  static readonly Port: number = Number.parseInt(process.env.PORT || "8000", 10);
  static readonly SecurePort: number = Number.parseInt(process.env.SECURE_PORT || "8443", 10);
  static readonly KeepAliveTimeout: number = Number.parseInt(process.env.SECURE_PORT || "0", 10);
  static readonly ConfigPath: string = process.env.CONFIG_PATH || "./config.yaml";

  static readonly Key: string = process.env.KEY || undefined;
  static readonly Cert: string = process.env.CERT || undefined;

  static readonly OnConnect: boolean = !!process.env.ON_CONNECT;
  static readonly OnDisconnect: boolean = !!process.env.ON_DISCONNECT;
  static readonly RequestQueueUrl: string = process.env.CONFIGURAPI_SQS_REQUEST_QUEUE;
  static readonly ErrorQueueUrl: string = process.env.CONFIGURAPI_SQS_ERROR_QUEUE;
  static readonly ResponseQueueUrl: string = process.env.CONFIGURAPI_SQS_RESPONSE_QUEUE;

  static readonly Region: string = process.env.AWS_REGION || "us-east-1";
  static readonly ConcurrencyCount: number = Number.parseInt(
    process.env.CONFIGURAPI_SQS_CONCURRENCY_COUNT || "10",
    10
  );

  static readonly CorrelationHeader: string = "x-provenance-id";

  static readonly DefaultEventName: string = process.env?.DEFAULT_EVENT_NAME || "";
}
